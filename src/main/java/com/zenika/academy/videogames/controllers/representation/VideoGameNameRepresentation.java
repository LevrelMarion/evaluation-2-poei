package com.zenika.academy.videogames.controllers.representation;

public class VideoGameNameRepresentation {
    private String name;

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }
}
