package com.zenika.academy.videogames.service;

import com.zenika.academy.videogames.service.rawg.RawgDatabaseClient;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;

import java.util.Scanner;

@Configuration

public class BeanConfig {

    @Bean
    public RawgDatabaseClient rawgDatabaseClient() {
        RawgDatabaseClient client = new RawgDatabaseClient();
        return client;
    }
}
