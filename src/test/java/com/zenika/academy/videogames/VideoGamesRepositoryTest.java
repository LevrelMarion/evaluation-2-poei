package com.zenika.academy.videogames;
import com.zenika.academy.videogames.domain.VideoGame;
import com.zenika.academy.videogames.repository.VideoGamesRepository;
import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.Test;
import java.util.List;

public class VideoGamesRepositoryTest {
    @Test
void doitDonnerTouteLaListeDesJeuxVideo () {
        VideoGame videoGame1 = new VideoGame( 12345L ,"Mario Kart",List.of());
        VideoGame videoGame2 = new VideoGame( 4567L ,"FF8",List.of());
        VideoGame videoGame3 = new VideoGame( 497657L ,"FF15",List.of());
        VideoGamesRepository videoGamesRepository = new VideoGamesRepository();
        videoGamesRepository.save(videoGame1);
     //   videoGamesRepository.save(videoGame2);
     //   videoGamesRepository.save(videoGame3);
        Assertions.assertEquals(videoGame1,videoGamesRepository.getAll().get(0));
    }
}

